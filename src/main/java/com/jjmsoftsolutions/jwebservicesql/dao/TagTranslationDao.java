package com.jjmsoftsolutions.jwebservicesql.dao;

import java.util.List;

import com.jjmsoftsolutions.jwebservicesql.dataAccess.GeneralDAO;
import com.jjmsoftsolutions.jwebservicesql.entity.dao.impl.Tag_Translation;

public interface TagTranslationDao extends GeneralDAO<Tag_Translation>{

	public List<Tag_Translation> findPrincipalTagsByLanguage(String language);
	public List<Tag_Translation> findTagsByLanguageAndTitle(String language,String title);
	public Tag_Translation findTagById(int id);
	
}
