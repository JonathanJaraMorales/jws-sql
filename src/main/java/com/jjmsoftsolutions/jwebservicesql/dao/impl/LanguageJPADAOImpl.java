package com.jjmsoftsolutions.jwebservicesql.dao.impl;

import java.util.HashMap;
import javax.inject.Named;

import com.jjmsoftsolutions.jwebservicesql.dao.LanguageDAO;
import com.jjmsoftsolutions.jwebservicesql.dataAccess.JPADAO;
import com.jjmsoftsolutions.jwebservicesql.dataAccess.JPAMetaData;
import com.jjmsoftsolutions.jwebservicesql.entity.dao.Language;

@Named
public class LanguageJPADAOImpl extends JPADAO<Language> implements LanguageDAO {

	public Language findByLanguageCode(String code) {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT P FROM ").append(JPAMetaData.LANGUAGE_TABLE_NAME).append(" P ");
		sql.append("WHERE P.code = :code");
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("code", code);
		return find(sql.toString(), parameters);
	}

	//FIXME
	public void deleteByCode(String code) {
		delete(JPAMetaData.LANGUAGE_TABLE, code);
	}
	
}
