package com.jjmsoftsolutions.jwebservicesql.dataAccess;


public interface DAO<T> extends GeneralDAO<T>{
	public int count();
}
