package com.jjmsoftsolutions.jwebservicesql.dataAccess;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface GeneralDAO<T> {
	public void insert(T entity);
	public T merge(T entity);
	public void deleteById(int id);
	public void delete(Class<?> clazz, Object key);
	public T findById(int id);
	public T find(String string, HashMap<String, Object> parameters);
	public List<T> findAll();
	public List<T> findAll(String sql, Map<String, Object> parameters);
	public List<T> findAll(String sql, Map<String, Object> parameters, int begin, int total);
	public T update(T t);
}
