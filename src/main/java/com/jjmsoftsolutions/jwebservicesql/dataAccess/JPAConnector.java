package com.jjmsoftsolutions.jwebservicesql.dataAccess;

import java.util.List;
import java.util.Map;

@Deprecated
public interface JPAConnector<E> {
	public E findById(int id, Class<E> clazz);
	public List<E> findAll(Class<E> clazz);
	public List<E> findAll(String sql, Map<String, Object> parameters,  Class<E> clazz);
	public List<E> findAll(String sql, Map<String, Object> parameters,  Class<E> clazz, int begin, int end);
	public E find(String sql, Map<String, Object> parameters, Class<E> clazz);
	public void delete(int id, Class<E> clazz);
	public boolean insert(E object);
	public E doInsert(E object);
	public int count(String sql, Map<String, Object> parameters, Class<E> clazz);

}
