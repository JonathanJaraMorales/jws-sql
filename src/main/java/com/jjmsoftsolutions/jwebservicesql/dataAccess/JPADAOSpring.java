package com.jjmsoftsolutions.jwebservicesql.dataAccess;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;

import org.springframework.transaction.annotation.Transactional;

//import com.jjmsoftsolutions.jcommon.exceptions.ConstraintException;
import com.jjmsoftsolutions.jcommon.exceptions.NotFoundException;

public class JPADAOSpring<T> /*implements GenericDAO<T>*/ {

	private Class<T> clazz;

	private EntityManager entityManager;

    //@PersistenceContext
    public void setEntityManager(EntityManager entityManager) {
        this. entityManager = entityManager;
    }
    
	@SuppressWarnings("unchecked")
	public JPADAOSpring() {
		Type t = getClass().getGenericSuperclass();
		ParameterizedType pt = (ParameterizedType) t;
		clazz = (Class<T>) pt.getActualTypeArguments()[0];
	}

	public int count(String sql, Map<String, Object> params) {
		return ((Long) getQuery(sql.toString(), params).getSingleResult())
				.intValue();
	}

	private TypedQuery<T> getQuery(String sql, Map<String, Object> parameters) {
		TypedQuery<T> query = entityManager.createQuery(sql, clazz);
		if (parameters != null) {
			Iterator<Entry<String, Object>> iterator = parameters.entrySet()
					.iterator();
			while (iterator.hasNext()) {
				Entry<String, Object> e = iterator.next();
				if (e.getValue() instanceof Date) {
					query.setParameter(e.getKey(), (Date) e.getValue(),
							TemporalType.DATE);
				} else {
					query.setParameter(e.getKey(), e.getValue());
				}
			}
		}
		return query;
	}

	@Transactional
	public void insert(T t) {
		try {
			entityManager.persist(t);
		} catch (Exception e) {
			//throw new ConstraintException();
		}
	}

	public void deleteById(int id) {
		T t = findById(id);
		entityManager.remove(t);
	}

	public T findById(int id) {
		T t = this.entityManager.find(clazz, id);
		if (t == null) {
			throw new NotFoundException();
		}
		return t;
	}

	public T find(String sql, HashMap<String, Object> parameters) {
		T t = null;
		try {
			t = getQuery(sql, parameters).getSingleResult();
		} catch (Exception e) {
			
		}

		return t;
	}

	public List<T> findAll() {
		StringBuilder sql = new StringBuilder();
		sql.append("Select U From ").append(clazz.getSimpleName())
				.append(" U ");
		return getQuery(sql.toString(), null).getResultList();
	}

	public T update(T t) {
		return entityManager.merge(t);
	}

	public List<T> findAll(String sql, Map<String, Object> parameters) {
		return getQuery(sql.toString(), parameters).getResultList();
	}

	public List<T> findAll(String sql, Map<String, Object> parameters,
			int begin, int total) {
		TypedQuery<T> query = getQuery(sql, parameters);
		query.setFirstResult(begin);
		query.setMaxResults(total);
		return query.getResultList();
	}
}
