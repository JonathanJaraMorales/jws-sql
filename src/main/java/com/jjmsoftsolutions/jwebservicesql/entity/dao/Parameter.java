package com.jjmsoftsolutions.jwebservicesql.entity.dao;

import java.io.Serializable;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import com.jjmsoftsolutions.jwebservicesql.entity.dao.impl.ParameterJPAImpl;

@JsonDeserialize(as = ParameterJPAImpl.class)
public interface Parameter extends Serializable{
	
	public String getCode();
	public void setCode(String code);

	public String getContent();
	public void setContent(String content);

}
