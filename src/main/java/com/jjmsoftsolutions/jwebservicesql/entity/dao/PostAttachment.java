package com.jjmsoftsolutions.jwebservicesql.entity.dao;

import java.io.Serializable;
import java.util.List;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import com.jjmsoftsolutions.jwebservicesql.entity.dao.impl.PostAttachmentJPAImpl;

@JsonDeserialize(contentAs=List.class, as=PostAttachmentJPAImpl.class)
@JsonSerialize(contentAs=List.class, as=PostAttachmentJPAImpl.class)
public interface PostAttachment extends Serializable{
	public int getId();
	public void setId(int id);
	
	public String getDescription();
	public void setDescription(String description);
	
	public String getName();
	public void setName(String name);
	
	public int getSize();
	public void setSize(int size);

	public String getType();
	public void setType(String type);

	public String getUrl();
	public void setUrl(String url);
	
	public Post getPost();
	public void setPost(Post post);
}
