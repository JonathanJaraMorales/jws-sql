package com.jjmsoftsolutions.jwebservicesql.entity.dao;

import java.io.Serializable;
import java.util.List;

import org.codehaus.jackson.map.annotate.JsonDeserialize;

import com.jjmsoftsolutions.jwebservicesql.entity.dao.impl.PostJPAImpl;
import com.jjmsoftsolutions.jwebservicesql.entity.dao.impl.SuperPostJPAImpl;

@JsonDeserialize(as = SuperPostJPAImpl.class)
public interface SuperPost extends Serializable {
	
	public int getId();
	public void setId(int id);

	public List<Post> getPost();
	public void setPost(List<Post> post);
	
	public List<Language> getLanguages();
	public void setLanguages(List<Language> languages);
	
	public String getPicture();
	public void setPicture(String picture);
	
	public List<PostVideo> getVideos();
	public void setVideos(List<PostVideo> videos);
	
	public void addVideo(PostVideo video);
	public void removeVideo(PostVideo video);
	
	public void addPost(Post post);
	public void removePost(Post post);
}
