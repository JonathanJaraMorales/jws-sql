package com.jjmsoftsolutions.jwebservicesql.entity.dao;

import java.io.Serializable;
import java.util.List;

import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.jjmsoftsolutions.jwebservicesql.entity.dao.impl.TagJPAImpl;

@JsonDeserialize(contentAs=List.class, as=TagJPAImpl.class)
@JsonSerialize(contentAs=List.class, as=TagJPAImpl.class)
public interface Tag extends Serializable {
	
	public Post getPost();
	public void setPost(Post post);

	public int getId();
	public void setId(int id);

	public String getName();
	public void setName(String name);
}
