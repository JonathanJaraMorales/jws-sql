package com.jjmsoftsolutions.jwebservicesql.entity.dao;

import java.io.Serializable;
import java.util.Date;

public interface User extends Serializable {

	public String getEmail();
	public void setEmail(String email);

	public String getAvailable();
	public void setAvailable(String available);

	public Date getCreatedDate();
	public void setCreatedDate(Date createdDate);

	public String getNickName();
	public void setNickName(String nickName);

	public String getPassword();
	public void setPassword(String password);

	public String getPhoto();
	public void setPhoto(String photo);
}
