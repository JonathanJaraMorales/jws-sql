package com.jjmsoftsolutions.jwebservicesql.entity.dao.impl;

import javax.inject.Named;
import javax.persistence.*;

import org.codehaus.jackson.annotate.JsonBackReference;
import org.codehaus.jackson.map.annotate.JsonDeserialize;

import com.jjmsoftsolutions.jwebservicesql.entity.dao.Language;
import com.jjmsoftsolutions.jwebservicesql.entity.dao.Post;

import java.util.List;

@Named
@Entity
@Table(name="Language")
public class LanguageJPAImpl implements Language {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="language_Code", nullable=false, length=2)
	private String code;

	@Column(name="name")
	private String name;

	@OneToMany(mappedBy="language", fetch=FetchType.LAZY, targetEntity = PostJPAImpl.class)
	@JsonBackReference
	@JsonDeserialize(contentAs=PostJPAImpl.class)
	private List<Post> post;
	
	@Column(name="enable")
	private int enable;

	public LanguageJPAImpl() {
	}


	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	
	public List<Post> getPost() {
		return this.post;
	}

	public void setPost(List<Post> post) {
		this.post = post;
	}
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}


	@Override
	public int getEnable() {
		return enable;
	}


	@Override
	public void setEnable(int enable) {
		this.enable = enable;
	}

}