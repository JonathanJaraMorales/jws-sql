package com.jjmsoftsolutions.jwebservicesql.entity.dao.impl;

import com.jjmsoftsolutions.jcommon.generic.EntityEror;
import javax.inject.Named;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.codehaus.jackson.annotate.JsonManagedReference;

import com.jjmsoftsolutions.jwebservicesql.entity.dao.Language;
import com.jjmsoftsolutions.jwebservicesql.entity.dao.Post;
import com.jjmsoftsolutions.jwebservicesql.entity.dao.PostAttachment;
import com.jjmsoftsolutions.jwebservicesql.entity.dao.SuperPost;
import com.jjmsoftsolutions.jwebservicesql.entity.dao.Tag;

import java.util.Date;
import java.util.List;

@Named
@Entity
@Table(name="Post_Translation")
public class PostJPAImpl implements Post {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id_Post_Translation")
	private int id;

	@Lob
	@NotNull(message = EntityEror.POST_CONTENT_NOT_NULL)
	@Column(name="content")
	private String content;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_Date")
	@NotNull
	private Date createDate;

	@Lob
	@NotNull(message = EntityEror.POST_DESCRIPTION_NOT_NULL)
	@Column(name="description")
	private String description;

	@Column(name = "title")
	@NotNull(message = EntityEror.POST_TITLE_NOT_NULL)
	@Size(min = 1, max = 150)
	private String title;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="update_Date")
	private Date updateDate;
	
	@Lob
	@Column(name="picture")
	@NotNull(message = EntityEror.POST_PICTURE_NOT_NULL)
	private String picture;

	@Column(name="views")
	private int views;
	
	@Column(name="likes")
	private int likes;
	
	@ManyToOne(cascade= {CascadeType.MERGE, CascadeType.REFRESH}, fetch = FetchType.EAGER, targetEntity = LanguageJPAImpl.class)
	@JoinColumn(name = "id_Language")
	@JsonManagedReference
	private Language language;
	
	@ManyToOne(cascade= {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH}, fetch = FetchType.EAGER, targetEntity = SuperPostJPAImpl.class)
	@JoinColumn(name = "id_Post")
	@JsonManagedReference
	private SuperPost superPost;
	
	@OneToMany(cascade= {CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.REMOVE}, fetch=FetchType.EAGER,  targetEntity = TagJPAImpl.class, mappedBy="post")
	@JsonManagedReference
	private List<Tag> tags;
	
	@OneToMany(cascade= {CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.REMOVE}, fetch=FetchType.EAGER,  targetEntity = PostAttachmentJPAImpl.class, mappedBy="post")
	@JsonManagedReference
	private List<PostAttachment> attachments;

	public PostJPAImpl() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getUpdateDate() {
		return this.updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	
	public int getViews() {
		return views;
	}

	public void setViews(int views) {
		this.views = views;
	}

	public int getLikes() {
		return likes;
	}

	public void setLikes(int likes) {
		this.likes = likes;
	}

	public Language getLanguage() {
		return this.language;
	}

	public void setLanguage(Language language) {
		this.language = language;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	
	public SuperPost getSuperPost() {
		return this.superPost;
	}

	public void setSuperPost(SuperPost superPost) {
		this.superPost = superPost;
	}
	
	public List<Tag> getTags() {
		return tags;
	}

	public void setTags(List<Tag> tags) {
		this.tags = tags;
	}
	
	public void addTag(Tag tag) {
		getTags().add(tag);
		tag.setPost(this);
	}

	public void removeTag(Tag tag) {
		getTags().remove(tag);
		tag.setPost(null);
	}

	@Override
	public List<PostAttachment> getAttachments() {
		return attachments;
	}

	@Override
	public void setAttachments(List<PostAttachment> attachments) {
		this.attachments = attachments;
	}

	@Override
	public void addAttachment(PostAttachment attachment) {
		getAttachments().add(attachment);
		attachment.setPost(this);
	}

	@Override
	public void removeAttachment(PostAttachment attachment) {
		getAttachments().remove(attachment);
		attachment.setPost(null);
		
	}

}