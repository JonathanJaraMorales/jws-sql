package com.jjmsoftsolutions.jwebservicesql.entity.dao.impl;


import javax.inject.Named;
import javax.persistence.*;

import org.codehaus.jackson.annotate.JsonBackReference;
import org.codehaus.jackson.annotate.JsonManagedReference;

import com.jjmsoftsolutions.jwebservicesql.entity.dao.Language;
import com.jjmsoftsolutions.jwebservicesql.entity.dao.Post;
import com.jjmsoftsolutions.jwebservicesql.entity.dao.PostVideo;
import com.jjmsoftsolutions.jwebservicesql.entity.dao.SuperPost;

import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "Post")
@Named
public class SuperPostJPAImpl implements SuperPost {
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_Post")
	private int id;

	@OneToMany(cascade=CascadeType.ALL, mappedBy = "superPost", fetch = FetchType.EAGER, targetEntity = PostJPAImpl.class)
	@JsonBackReference
	private List<Post> post;

	@JsonManagedReference
	@ManyToMany(cascade = CascadeType.ALL, fetch=FetchType.EAGER, targetEntity = LanguageJPAImpl.class)
	@JoinTable(name = "Post_Translation",  joinColumns = { @JoinColumn(name = "id_Post", referencedColumnName = "id_Post") },  inverseJoinColumns = { @JoinColumn(name = "id_Language", referencedColumnName = "language_Code") })
	private List<Language> languages;

	@Lob
	@Column(name="picture")
	private String picture;
	
	@OneToMany(cascade= {CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.REMOVE}, fetch=FetchType.EAGER,  targetEntity = PostVideoJPAImpl.class, mappedBy="superPost")
	@JsonManagedReference
	private List<PostVideo> videos;
	
	public SuperPostJPAImpl() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public List<Post> getPost() {
		return this.post;
	}

	public void setPost(List<Post> post) {
		this.post = post;
	}

	public List<Language> getLanguages() {
		return languages;
	}

	public void setLanguages(List<Language> languages) {
		this.languages = languages;
	}

	@Override
	public String getPicture() {
		return picture;
	}

	@Override
	public void setPicture(String picture) {
		this.picture = picture;
	}

	@Override
	public List<PostVideo> getVideos() {
		return videos;
	}

	@Override
	public void setVideos(List<PostVideo> videos) {
		this.videos = videos;
	}

	@Override
	public void addVideo(PostVideo video) {
		getVideos().add(video);
		video.setSuperPost(this);
	}

	@Override
	public void removeVideo(PostVideo video) {
		getVideos().remove(video);
		video.setSuperPost(null);
	}

	@Override
	public void addPost(Post post) {
		if(getPost() == null)
			setPost(new ArrayList<Post>());
		getPost().add(post);
		post.setSuperPost(this);
	}

	@Override
	public void removePost(Post post) {
		getPost().remove(post);
		post.setSuperPost(null);
	}

}