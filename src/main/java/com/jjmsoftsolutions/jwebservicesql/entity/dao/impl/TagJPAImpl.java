package com.jjmsoftsolutions.jwebservicesql.entity.dao.impl;

import javax.inject.Named;
import javax.persistence.*;

import org.codehaus.jackson.annotate.JsonBackReference;

import com.jjmsoftsolutions.jwebservicesql.entity.dao.Post;
import com.jjmsoftsolutions.jwebservicesql.entity.dao.Tag;

@Named
@Entity
@Table(name = "Tag")
public class TagJPAImpl implements Tag {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name="id_Tag")
	private int id;

	@Column(name="name")
	private String name;

	@ManyToOne(cascade = CascadeType.ALL, targetEntity = PostJPAImpl.class)
	@JoinColumn(name = "id_Post_Translation")
	@JsonBackReference
	private Post post;

	public TagJPAImpl() {
	}
	
	public Post getPost() {
		return post;
	}

	public void setPost(Post post) {
		this.post = post;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}