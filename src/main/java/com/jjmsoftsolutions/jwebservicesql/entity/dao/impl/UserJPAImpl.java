package com.jjmsoftsolutions.jwebservicesql.entity.dao.impl;

import javax.inject.Named;
import javax.persistence.*;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.jjmsoftsolutions.jwebservicesql.entity.dao.User;

import java.util.Date;

@Named
@Entity
@Table(name = "User")
public class UserJPAImpl implements User {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "email")
	private String email;

	@Column(name = "available")
	@JsonIgnore
	private String available;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_Date")
	@JsonIgnore
	private Date createdDate;

	@Column(name = "nick_Name")
	private String nickName;

	@Column(name = "password")
	@JsonIgnore
	private String password;

	@Lob
	@Column(name = "photo")
	private String photo;

	public UserJPAImpl() {
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAvailable() {
		return this.available;
	}

	public void setAvailable(String available) {
		this.available = available;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getNickName() {
		return this.nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPhoto() {
		return this.photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

}