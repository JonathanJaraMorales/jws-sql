package com.jjmsoftsolutions.jwebservicesql.entity.parser.dao;

import com.jjmsoftsolutions.jwebservicesql.entity.dao.Parameter;

public interface ParameterParse {
	public Parameter jsonToParameter(String json);
}
