package com.jjmsoftsolutions.jwebservicesql.entity.parser.dao;

import com.jjmsoftsolutions.jwebservicesql.entity.dao.User;

public interface UserParser {
	public User jsonToUser(String json);
}
