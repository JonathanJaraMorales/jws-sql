package com.jjmsoftsolutions.jwebservicesql.entity.parser.dao.impl;

import javax.inject.Inject;
import javax.inject.Named;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.jjmsoftsolutions.jwebservicesql.entity.dao.Parameter;
import com.jjmsoftsolutions.jwebservicesql.entity.parser.dao.ParameterParse;

@Named
public class ParameterParserImpl implements ParameterParse {

	@Inject private Parameter parameter;
	
	public Parameter jsonToParameter(String json) {
		JsonParser jsonParser = new JsonParser();
		JsonObject jsonObject = (JsonObject)jsonParser.parse(json);
		String code = jsonObject.get("code").getAsString();
		String content = jsonObject.get("content").getAsString();
		parameter.setCode(code);
		parameter.setContent(content);
		return parameter;
	}

}
