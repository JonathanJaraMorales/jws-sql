package com.jjmsoftsolutions.jwebservicesql.entity.parser.dao.impl;

import java.util.Date;

import javax.inject.Inject;
import javax.inject.Named;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.jjmsoftsolutions.jcommon.utils.Crypto;
import com.jjmsoftsolutions.jwebservicesql.entity.dao.User;
import com.jjmsoftsolutions.jwebservicesql.entity.parser.dao.UserParser;

@Named
public class UserParserImpl implements UserParser {

	@Inject private User user;
	
	@Override
	public User jsonToUser(String jsonRequest) {
		JsonParser jsonParser = new JsonParser();
		JsonObject jsonObject = (JsonObject)jsonParser.parse(jsonRequest);
		setUser(jsonObject);
		return user;
	}

	private void setUser(JsonObject jsonObject) {
		String nickName = jsonObject.get("nickName").getAsString();
		String password = jsonObject.get("password").getAsString();
		String email = jsonObject.get("email").getAsString();
		
		user.setAvailable("T");
		user.setCreatedDate(new Date());
		user.setNickName(nickName);
		user.setPassword(Crypto.md5(password));
		user.setEmail(email);
		
	}

}
