package com.jjmsoftsolutions.jwebservicesql.service;

import java.util.List;
import javax.inject.Inject;
import org.glassfish.hk2.api.DynamicConfiguration;
import org.glassfish.hk2.api.Factory;
import org.glassfish.hk2.api.ServiceLocator;
import org.glassfish.hk2.utilities.binding.ServiceBindingBuilder;
import org.glassfish.jersey.internal.inject.Injections;
import org.glassfish.jersey.server.ResourceConfig;
import com.jjmsoftsolutions.jcommon.generic.ClassFinder;

public class ApplicationG extends ResourceConfig {

	@Inject
	public ApplicationG(ServiceLocator locator) throws InstantiationException, IllegalAccessException {
		super();
		DynamicConfiguration c = Injections.getConfiguration(locator);
		inject(locator, c, "com.jjmsoftsolutions.jwebservicesql.service.impl", "com.jjmsoftsolutions.jwebservicesql.dao.impl", "com.jjmsoftsolutions.jwebservicesql.entity.dao.impl","com.jjmsoftsolutions.jwebservicesql.entity.parser.dao.impl");
		c.commit();
	}
	
	
	
	private void inject(ServiceLocator locator, DynamicConfiguration c, String... packagesNames) throws InstantiationException, IllegalAccessException{
		for(String packageName : packagesNames){
			List<Class<?>> implementations = ClassFinder.find(packageName);
			for(Class<?> implementation : implementations){
				Object implementationInstance = implementation.newInstance();
				ServiceBindingBuilder<Object> bindinBuilder = Injections.newFactoryBinder(new BeanFactory(locator, implementationInstance));
				Class<?>[] interfacesDAO = implementation.getInterfaces();
				for(Class<?> interfaceDAO : interfacesDAO){
					bindinBuilder.to(interfaceDAO);
				}
				Injections.addBinding(bindinBuilder, c);
			}
		}
	}
	
	private static class BeanFactory implements Factory<Object> {

		private ServiceLocator locator;
		private Object bean;

		public BeanFactory(ServiceLocator locator, Object bean) {
			this.locator = locator;
			this.bean = bean;
		}

		@Override
		public Object provide() {
			locator.inject(bean);
			return bean;
		}

		@Override
		public void dispose(Object instance) {
		}

	}

}