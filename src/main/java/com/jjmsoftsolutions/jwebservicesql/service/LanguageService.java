package com.jjmsoftsolutions.jwebservicesql.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.jjmsoftsolutions.jcommon.rest.ServicePathName;

public interface LanguageService {
	
	@POST
	@Path(ServicePathName.LANGUAGE_INSERT)
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response insert(String jsonRequest);

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path(ServicePathName.LANGUAGE_FIND_BY_CODE+"/{code}")
	public Response findByLanguageCode(@PathParam("code") final String code);
	
	@DELETE
	@Path(ServicePathName.LANGUAGE_DELETE_BY_CODE+"/{code}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response deleteByCode(@PathParam("code") final String code);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path(ServicePathName.LANGUAGE_FIND_ALL)
	public Response findAll();
}
