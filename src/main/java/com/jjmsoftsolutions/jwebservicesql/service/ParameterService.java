package com.jjmsoftsolutions.jwebservicesql.service;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.jjmsoftsolutions.jcommon.rest.ServicePathName;
import com.jjmsoftsolutions.jwebservicesql.entity.dao.Parameter;

public interface ParameterService {

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path(ServicePathName.PARAMETER_FIND_BY_CODE+"/{code}")
	public Parameter findByCode(@PathParam("code") final String code);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path(ServicePathName.PARAMETER_FIND_ALL)
	public List<Parameter> findAll();
	
	@POST
	@Path(ServicePathName.PARAMETER_UPDATE)
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response update(String parameter);
}
