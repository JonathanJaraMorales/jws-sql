package com.jjmsoftsolutions.jwebservicesql.service;

import java.io.Serializable;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.jjmsoftsolutions.jcommon.rest.ServicePathName;

public interface UserService extends Serializable {

	@POST
	@Path(ServicePathName.USER_INSERT)
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response insert(String post);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path(ServicePathName.USER_FIND_BY_EMAIL_PASSWORD + "/{email}/{password}")
	public Response findByEmailPassword(@PathParam("email") final String email,  @PathParam("password") final String password);

}
