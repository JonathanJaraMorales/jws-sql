package com.jjmsoftsolutions.jwebservicesql.service.impl;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import org.springframework.stereotype.Component;
import com.jjmsoftsolutions.jcommon.rest.ServiceName;
import com.jjmsoftsolutions.jwebservicesql.dao.ParameterDAO;
import com.jjmsoftsolutions.jwebservicesql.entity.dao.Parameter;
import com.jjmsoftsolutions.jwebservicesql.entity.parser.dao.ParameterParse;
import com.jjmsoftsolutions.jwebservicesql.service.ParameterService;

@Path(ServiceName.PARAMETER)
@Component
public class ParameterServiceImpl implements ParameterService {

	@Inject private ParameterDAO dao;
	@Inject private ParameterParse parser;
	
	public Parameter findByCode(String code) {
		return dao.findByCode(code);
	}

	@Override
	public List<Parameter> findAll() {
		return dao.findAll();
	}

	@Override
	public Response update(String json) {
		Parameter parameter = parser.jsonToParameter(json);
		Parameter parameterToUpdate = dao.findByCode(parameter.getCode());
		parameterToUpdate.setContent(parameter.getContent());
		dao.merge(parameterToUpdate);
		return null;
	}
}
