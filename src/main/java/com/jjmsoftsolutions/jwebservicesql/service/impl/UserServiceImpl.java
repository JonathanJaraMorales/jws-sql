package com.jjmsoftsolutions.jwebservicesql.service.impl;

import javax.inject.Inject;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.stereotype.Component;

import com.jjmsoftsolutions.jcommon.exceptions.ConstraintException;
import com.jjmsoftsolutions.jcommon.exceptions.NotFoundException;
import com.jjmsoftsolutions.jcommon.rest.ServiceName;
import com.jjmsoftsolutions.jwebservicesql.dao.UserDAO;
import com.jjmsoftsolutions.jwebservicesql.entity.dao.User;
import com.jjmsoftsolutions.jwebservicesql.entity.dao.impl.UserJPAImpl;
import com.jjmsoftsolutions.jwebservicesql.entity.parser.dao.UserParser;
import com.jjmsoftsolutions.jwebservicesql.service.Service;
import com.jjmsoftsolutions.jwebservicesql.service.UserService;
import com.jjmsoftsolutions.jcommon.generic.Error;

@Path(ServiceName.USER)
@Component
public class UserServiceImpl extends Service<UserJPAImpl> implements UserService {

	private static final long serialVersionUID = 1L;

	@Inject private UserDAO dao;
	@Inject private UserParser parser;
	
	@Override
	public Response insert(String jsonRequest) {
		try {
			User user = parser.jsonToUser(jsonRequest);
			dao.insert(user);
			return Response.ok().build();	
		} catch (ConstraintException e) {
			return Response.status(Error.USER_INSERT.getCode()).entity(Error.USER_INSERT.getMessage()).type(MediaType.APPLICATION_JSON).build();
		}
	}

	@Override
	public Response findByEmailPassword(String email, String password) {
		try {
			User user = dao.findByEmailPassword(email.trim(), password.trim());
			return Response.ok().entity(user).type(MediaType.APPLICATION_JSON).build();
		} catch (NotFoundException e) {
			return Response.status(Error.USER_FIND_BY_EMAIL_PASSWORD.getCode()).entity(Error.USER_FIND_BY_EMAIL_PASSWORD.getMessage()).type(MediaType.APPLICATION_JSON).build();
		} 
	}


}
