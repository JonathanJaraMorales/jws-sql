CREATE TABLE IF NOT EXISTS Post_Attachments (
  id_Post_Attachments INT NOT NULL AUTO_INCREMENT,
  id_Post_Translation INT NOT NULL,
  name VARCHAR(50) NULL,
  description VARCHAR(150) NULL,
  url LONGTEXT NULL,
  size INT NULL,
  type VARCHAR(50) NULL,
  PRIMARY KEY (idPost_Attachments),
  INDEX FK_Post_Attachment_Post_Attachments_idx (id_Post_Translation ASC),
  CONSTRAINT FK_Post_Attachment_Post_Attachments
    FOREIGN KEY (id_Post_Translation)
    REFERENCES Post_Translation (id_Post_Translation)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;