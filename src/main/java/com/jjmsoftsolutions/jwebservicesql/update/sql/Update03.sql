CREATE TABLE IF NOT EXISTS Post_Video (
  id_Post_Video INT NOT NULL AUTO_INCREMENT,
  id_Post INT NOT NULL,
  type VARCHAR(25) NOT NULL,
  url LONGTEXT NOT NULL,
  created_Date DATETIME NOT NULL,
  PRIMARY KEY (id_Post_Video),
  INDEX FK_Post_Video_Post_idx (id_Post ASC),
  CONSTRAINT FK_Post_Video_Post
    FOREIGN KEY (id_Post)
    REFERENCES Post (id_Post)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;