package com.jjmsoftsolutions.jwebservicesql.update.version;

import com.jjmsoftsolutions.jdbupdater.update.Update;
import com.jjmsoftsolutions.jwebservicesql.update.framework.Updater;

public class Update03 extends Updater implements Update {

	public String execute() {
		 return getScript("Update03.sql");
	}

}
