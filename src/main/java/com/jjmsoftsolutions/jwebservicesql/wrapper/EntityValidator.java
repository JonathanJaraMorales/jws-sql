package com.jjmsoftsolutions.jwebservicesql.wrapper;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

import javax.validation.constraints.NotNull;

import com.jjmsoftsolutions.jcommon.exceptions.EntityValidatorException;

public class EntityValidator {

	public static boolean processAnnotations(Object obj) {
        try {
            Class<? extends Object> cl = obj.getClass();

            // Checking all the fields for annotations
            for(Field f : cl.getDeclaredFields()) {
            	 for(Annotation a : f.getAnnotations()) {
                     // Checking for a NullValueValidate annotation
                     if(a.annotationType() == NotNull.class) {
                     	NotNull nullVal = (NotNull) a;
                         
                         // Setting the field to be accessible from our class
                         // is it is a private member of the class under processing
                         // (which its most likely going to be)
                         // The setAccessible method will not work if you have
                         // Java SecurityManager configured and active.
                         f.setAccessible(true);

                         // Checking the field for a null value and
                         // throwing an exception is a null value encountered.
                         // The get(Object obj) method on Field class returns the
                         // value of the Field for the Object which is under test right now.
                         // In other words, we need to send 'obj' as the object
                         // to this method since we are currently processing the
                         // annotations present on the 'obj' Object.
                         if(f.get(obj) == null) {
                         	throw new EntityValidatorException(99, nullVal.message());
                         }
                     }
                 }
            }
        } catch(Exception e) {
        	throw new EntityValidatorException(99, e.getMessage());
        }
		return true;
    }
}
