package com.jjmsoftsolutions.jwebservicesql.wrapper;

import com.jjmsoftsolutions.jcommon.json.JacksonConverter;

public class JSonConverter<T> {
	
	public com.jjmsoftsolutions.jcommon.json.JSonConverter<T> getInstance(){
		return new JacksonConverter<T>();
	}

}
