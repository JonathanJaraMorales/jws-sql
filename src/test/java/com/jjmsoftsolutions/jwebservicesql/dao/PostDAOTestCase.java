package com.jjmsoftsolutions.jwebservicesql.dao;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.jjmsoftsolutions.jcommon.exceptions.NotFoundException;
import com.jjmsoftsolutions.jwebservicesql.dataAccess.GeneralDAO;
import com.jjmsoftsolutions.jwebservicesql.entity.dao.Language;
import com.jjmsoftsolutions.jwebservicesql.entity.dao.Post;
import com.jjmsoftsolutions.jwebservicesql.test.factory.JPATestFactory;

public class PostDAOTestCase extends JPATestFactory {

	private PostDAO dao;

	@Override
	public GeneralDAO<?> getConfig() {
		dao = newInstance(PostDAO.class);
		return dao;
	}

	@Test
	public void findByLanguage_canFindPostByLanguage_ExpectedListOfPost() {		
		Language language = newInstance(Language.class);
		language.setCode("ES");
		language.setEnable(1);
		language.setName("Spanish");
		buildAndSave(language);
		
		Post post1 = newInstance(Post.class);
		post1.setLanguage(language);
		buildAndSave(post1);
		
		Assert.assertEquals(1, dao.findByLanguage(language.getCode(), 0, 2).size());		
	}
	
	@Test
	public void findByLanguage_canFindPostByLanguage_ExpectedListSorted(){
		Language language = newInstance(Language.class);
		language.setCode("GR");
		language.setEnable(1);
		language.setName("German");
		buildAndSave(language);
		
		Post post = newInstance(Post.class);
		post.setId(1);
		post.setLanguage(language);
		buildAndSave(post);
	
		Post post2 = newInstance(Post.class);
		post2.setId(2);
		post2.setLanguage(language);
		buildAndSave(post2);
		
		List<Post> postList = dao.findByLanguage(language.getCode(), 0, 2);
		Assert.assertEquals(2, postList.size());
		Assert.assertEquals(post2.getId(), postList.get(0).getId());
		Assert.assertEquals(post.getId(), postList.get(1).getId());
	}
	
	@Test
	public void findByLanguage_canFindPostByLanguage_ExpectedListWithPaginationByEnd() {
		Language language = newInstance(Language.class);
		language.setCode("GR");
		language.setEnable(1);
		language.setName("German");
		buildAndSave(language);
		
		Post post = newInstance(Post.class);
		post.setId(1);
		post.setLanguage(language);
		buildAndSave(post);
	
		Post post2 = newInstance(Post.class);
		post2.setId(2);
		post2.setLanguage(language);
		buildAndSave(post2);
		
		Post post3 = newInstance(Post.class);
		post3.setId(3);
		post3.setLanguage(language);
		buildAndSave(post3);
		
		List<Post> postList = dao.findByLanguage(language.getCode(), 0, 2);
		Assert.assertEquals(2, postList.size());
		Assert.assertEquals(3, postList.get(0).getId());
		Assert.assertEquals(post2.getId(), postList.get(1).getId());
	}
	
	@Test
	public void findByLanguage_canFindPostByLanguage_ExpectedListWithPaginationByBegin() {
		Language language = newInstance(Language.class);
		language.setCode("GR");
		language.setEnable(1);
		language.setName("German");
		buildAndSave(language);
		
		Post post1 = newInstance(Post.class);
		post1.setId(1);
		post1.setLanguage(language);
		buildAndSave(post1);
	
		Post post2 = newInstance(Post.class);
		post2.setId(2);
		post2.setLanguage(language);
		buildAndSave(post2);
		
		Post post3 = newInstance(Post.class);
		post3.setId(3);
		post3.setLanguage(language);
		buildAndSave(post3);
		
		List<Post> postList = dao.findByLanguage(language.getCode(), 1, 2);
		Assert.assertEquals(2, postList.size());
		Assert.assertEquals(post2.getId(), postList.get(0).getId());
		Assert.assertEquals(post1.getId(), postList.get(1).getId());
	}
	
	@Test
	public void findByLanguage_canFindUnregisterPostByLanguage_ExpectedNoResultException() {
		exception.expect(NotFoundException.class);
		dao.findByLanguage("ES", 0,1);
	}

}
