package com.jjmsoftsolutions.jwebservicesql.dao;

import org.junit.Assert;
import org.junit.Test;

import com.jjmsoftsolutions.jcommon.exceptions.NotFoundException;
import com.jjmsoftsolutions.jwebservicesql.dataAccess.GeneralDAO;
import com.jjmsoftsolutions.jwebservicesql.entity.dao.Tag;
import com.jjmsoftsolutions.jwebservicesql.test.factory.JPATestFactory;

public class TagDAOTestCase extends JPATestFactory {

	private TagDAO dao;
	
	@Override
	public GeneralDAO<?> getConfig() {
		dao = newInstance(TagDAO.class);
		return dao;
	}
	
	@Test
	public void findTagsByName_canFindTagsByName_ExpectedTag() {	
		Tag tag = newInstance(Tag.class);
		tag.setId(1);
		tag.setName("Java");
		buildAndSave(tag);
		Assert.assertEquals(tag.getName(), dao.findTagsByName("Java", 0, 1).get(0).getName());
	}

	@Test
	public void findTagsByName_canFindTagsByName_ExpectedNoResultException() {	
		exception.expect(NotFoundException.class);
		Assert.assertEquals("Java", dao.findTagsByName("Java", 0, 1).get(0).getName());
	}
}