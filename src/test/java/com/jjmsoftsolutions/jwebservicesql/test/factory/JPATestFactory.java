package com.jjmsoftsolutions.jwebservicesql.test.factory;

import java.util.HashMap;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.RollbackException;
import org.eclipse.persistence.exceptions.DatabaseException;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.rules.ExpectedException;
import org.springframework.test.util.ReflectionTestUtils;
import com.jjmsoftsolutions.jcommon.exceptions.ConstraintException;
import com.jjmsoftsolutions.jwebservicesql.dataAccess.GeneralDAO;
import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

public abstract class JPATestFactory extends JerseyTestG {

	@Rule public ExpectedException exception = ExpectedException.none();

	private GeneralDAO<?> dao = null;
	private EntityManagerFactory emf = null;
	private EntityManager em = null;

	public abstract GeneralDAO<?> getConfig();

	public JPATestFactory() {
		dao = getConfig();
		em = getEntityManagerFactory().createEntityManager();
		ReflectionTestUtils.setField(dao, "em", em);
		ReflectionTestUtils.setField(dao, "emf", emf);
	}

	public EntityManagerFactory getEntityManagerFactory() {
		Map<String, String> map = new HashMap<String, String>();
		map.put("eclipselink.ddl-generation.output-mode", "database");
		map.put("eclipselink.ddl-generation", "create-tables");
		map.put("jjavax.persistence.jdbc.url", "jdbc:hsqldb:." + Math.random());
		map.put("javax.persistence.jdbc.user", "sa");
		map.put("javax.persistence.jdbc.driver", "org.hsqldb.jdbcDriver");
		map.put("javax.persistence.jdbc.password", "");
		emf = Persistence.createEntityManagerFactory("TestDataModel", map);
		return emf;
	}

	public void buildAndSave(Object entity) {
		EntityManager em = null;
		try {
			em = getEntityManager();
			em.getTransaction().begin();
			em.persist(entity);
			em.getTransaction().commit();
		} catch (RollbackException ex) {
			final DatabaseException re = (DatabaseException) ex.getCause();
			if (re.getCause() != null
					&& re.getCause() instanceof MySQLIntegrityConstraintViolationException) {
				throw new ConstraintException();
			}
			if (em.getTransaction() != null && em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
		} finally {
			em.close();
		}
	}

	@After
	public void clear() {
		EntityManager em = getEntityManager();
		em.getTransaction().begin();
		em.createNativeQuery("TRUNCATE SCHEMA PUBLIC RESTART IDENTITY AND COMMIT NO CHECK").executeUpdate();
		em.getTransaction().commit();
	}

	@Before
	public void setUp() {}

	private EntityManager getEntityManager() {
		EntityManagerFactory factory = getEntityManagerFactory();
		em = factory.createEntityManager();
		return em;
	}

}
