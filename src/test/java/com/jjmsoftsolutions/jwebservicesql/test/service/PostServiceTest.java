package com.jjmsoftsolutions.jwebservicesql.test.service;

import java.util.Arrays;

import javax.inject.Inject;
import javax.ws.rs.core.Response;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.mockito.Mockito;
import org.springframework.test.util.ReflectionTestUtils;

import static org.mockito.Matchers.any;

import com.google.gson.Gson;
import com.jjmsoftsolutions.jcommon.exceptions.NotFoundException;
import com.jjmsoftsolutions.jcommon.generic.EntityEror;
import com.jjmsoftsolutions.jcommon.generic.Error;
import com.jjmsoftsolutions.jwebservicesql.dao.PostDAO;
import com.jjmsoftsolutions.jwebservicesql.entity.dao.Language;
import com.jjmsoftsolutions.jwebservicesql.entity.dao.Parameter;
import com.jjmsoftsolutions.jwebservicesql.entity.dao.Post;
import com.jjmsoftsolutions.jwebservicesql.service.ParameterService;
import com.jjmsoftsolutions.jwebservicesql.service.PostService;
import com.jjmsoftsolutions.jwebservicesql.test.factory.JerseyTestG;

@FixMethodOrder(MethodSorters.JVM)
public class PostServiceTest extends JerseyTestG {

	@Inject private PostService service;
	private ParameterService parameterService;
	private PostDAO dao;
	@Inject private Post post;
	
	@Before
	public void setUp(){
		dao = Mockito.mock(PostDAO.class);
		parameterService = Mockito.mock(ParameterService.class);
		
		ReflectionTestUtils.setField(service, "dao", dao);
		ReflectionTestUtils.setField(service, "parameterService", parameterService);		
	}
	
	@Test
	public void findByIdAutoIncrementCountViews(){
		Parameter parameter = newInstance(Parameter.class);
		parameter.setCode("POST_INFO_AUTO_INCREMENT_COUNT_VIEWS");
		parameter.setContent("1");
		
		Mockito.when(dao.findById(any(Integer.class))).thenReturn(post);
		Mockito.when(parameterService.findByCode("POST_INFO_AUTO_INCREMENT_COUNT_VIEWS")).thenReturn(parameter);
		Mockito.when(dao.merge(post)).thenReturn(post);
		
		Response response = service.findById(any(Integer.class));
		
		assertEquals(200, response.getStatus());
		assertEquals(1, cast(response.getEntity(), Post.class).getViews());
	}
	
	@Test
	public void findByIdWithoutAutoIncrementCountViews(){
		Parameter parameter = newInstance(Parameter.class);
		parameter.setCode("POST_INFO_AUTO_INCREMENT_COUNT_VIEWS");
		parameter.setContent("0");
		
		Mockito.when(dao.findById(any(Integer.class))).thenReturn(post);
		Mockito.when(parameterService.findByCode("POST_INFO_AUTO_INCREMENT_COUNT_VIEWS")).thenReturn(parameter);
		Mockito.when(dao.merge(post)).thenReturn(post);
		
		Response response = service.findById(any(Integer.class));
		
		assertEquals(200, response.getStatus());
		assertEquals(1, cast(response.getEntity(), Post.class).getViews());
	}
	
	@Test
	public void findByIdNotFoundException(){
		Mockito.when(dao.findById(any(Integer.class))).thenThrow(new NotFoundException());
		Response response = service.findById(any(Integer.class));		
		assertEquals(Error.POST_FIND_BY_ID.getCode(), response.getStatus());
		assertEquals(Error.POST_FIND_BY_ID.getMessage(), cast(response.getEntity(), String.class));
	}
	
	@Test
	public void findByLanguage(){
		Mockito.when(dao.findByLanguage(any(String.class), any(Integer.class), any(Integer.class))).thenReturn(Arrays.asList(post));
		Response response = service.findByLanguage(any(String.class), any(Integer.class), any(Integer.class));
		assertEquals(200, response.getStatus());
	}
	
	@Test
	public void findByLanguageNotFoundException(){
		Mockito.when(dao.findByLanguage(any(String.class), any(Integer.class), any(Integer.class))).thenThrow(new NotFoundException());
		Response response = service.findByLanguage(any(String.class), any(Integer.class), any(Integer.class));
		assertEquals(Error.POST_FIND_BY_LANGUAGE.getCode(), response.getStatus());
		assertEquals(Error.POST_FIND_BY_LANGUAGE.getMessage(), cast(response.getEntity(), String.class));
	}
	
	@Test 
	public void insertEntityValidatorException(){
		Post post = newInstance(Post.class);
		Language language = newInstance(Language.class);
		Response response = null;
		
		post.setContent(null);
		post.setCreateDate(null);
		post.setDescription(null);
		post.setLanguage(null);
		post.setPicture(null);
		post.setSuperPost(null);
		post.setTags(null);
		post.setTitle(null);
		post.setUpdateDate(null);
		
		response = service.insert(new Gson().toJson(post));
		assertEquals(Error.POST_INSERT_VALIDATION_LANGUAGE.getMessage(), cast(response.getEntity(), String.class));
		assertEquals(Error.POST_INSERT_VALIDATION_LANGUAGE.getCode(), response.getStatus());
		
		language.setCode("EN");
		post.setLanguage(language);
		response = service.insert(new Gson().toJson(post));
		assertEquals(EntityEror.POST_CONTENT_NOT_NULL, cast(response.getEntity(), String.class));
		
		post.setContent("");
		response = service.insert(new Gson().toJson(post));
		assertEquals(EntityEror.POST_DESCRIPTION_NOT_NULL, cast(response.getEntity(), String.class));
		
		post.setDescription("");
		response = service.insert(new Gson().toJson(post));
		assertEquals(EntityEror.POST_TITLE_NOT_NULL, cast(response.getEntity(), String.class));
		
		post.setTitle("");
		response = service.insert(new Gson().toJson(post));
		assertEquals(EntityEror.POST_PICTURE_NOT_NULL, cast(response.getEntity(), String.class));
		
		
		
		
	}
}
